﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp5
{
    class Program
    {
        static int Determinant(List<List<int>> matrix)
        {
            return ((matrix[0][0] * matrix[1][1] * matrix[2][2]) + (matrix[0][1] * matrix[1][2] * matrix[2][0]) + (matrix[0][2] * matrix[1][0] * matrix[2][1]) - (matrix[0][2] * matrix[1][1] * matrix[2][0]) - (matrix[0][1] * matrix[1][0] * matrix[2][2]) - (matrix[0][0] * matrix[1][2] * matrix[2][1]));
        }

        static void TransposedMatrix(List<List<double>> matrix)
        {
            double cock;
            for (int i = 0; i < matrix.Count; ++i)
            {
                for (int j = i + 1; j < matrix[i].Count; ++j)
                {
                    cock = matrix[i][j];
                    matrix[i][j] = matrix[j][i];
                    matrix[j][i] = cock;
                }
            }
        }
        static double GetMinor(List<List<int>> matrix, int i, int j)
        {
            if (i == 0 && j == 0) return (matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1]);
            if (i == 0 && j == 1) return (matrix[1][0] * matrix[2][2] - matrix[1][2] * matrix[0][2]);
            if (i == 0 && j == 2) return (matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]);
            if (i == 1 && j == 0) return (matrix[0][1] * matrix[2][2] - matrix[0][2] * matrix[2][1]);
            if (i == 1 && j == 1) return (matrix[0][0] * matrix[2][2] - matrix[0][2] * matrix[2][0]);
            if (i == 1 && j == 2) return (matrix[0][0] * matrix[2][1] - matrix[0][1] * matrix[2][0]);
            if (i == 2 && j == 0) return (matrix[0][1] * matrix[1][2] - matrix[0][2] * matrix[1][1]);
            if (i == 2 && j == 1) return (matrix[0][0] * matrix[1][2] - matrix[0][2] * matrix[1][0]);
                                  return (matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]);
        }

        static List<List<double>> InverseMatrix(List<List<int>> matrix)
        {
            List<List<double>> inversed = new List<List<double>>();
            List<double> temp;
            if (Determinant(matrix) == 0)
                return (null);
            for (int i = 0; i < 3; ++i)
            {
                temp = new List<double>();
                for (int j = 0; j < 3; ++j)
                    temp.Add(Math.Pow(-1, i + j) * (GetMinor(matrix, i, j) / Determinant(matrix)));
                inversed.Add(temp);
            }
            TransposedMatrix(inversed);
            return (inversed);
        }
        static void Main(string[] args)
        {
            List<List<int>> matrix = new List<List<int>>();
            List<int> temp;
            var rand = new Random();
            for (int i = 0; i < 3; ++i)
            {
                temp = new List<int>();
                for (int j = 0; j < 3; ++j)
                    temp.Add(rand.Next(0, 11));
                matrix.Add(temp);
            }
            Console.WriteLine(Determinant(matrix));
            foreach (var i in matrix)
            {
                foreach (var j in i)
                    Console.Write(j + " ");
                Console.WriteLine();
            }
            Console.WriteLine();
            List<List<double>> inversed = InverseMatrix(matrix);
            if (inversed == null)
                Console.WriteLine("нi");
            else
                foreach (var i in inversed)
                {
                    foreach (var j in i)
                        Console.Write(j + " ");
                    Console.WriteLine();
                }
        }
    }
}
