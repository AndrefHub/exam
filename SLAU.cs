﻿using System;
using System.Collections.Generic;
using System.Text;

namespace матрицы
{
     class Matrix
    {

        private double[,] data;

        public int m;
        public int M { get => this.m; }

        public int n;
        public int N { get => this.n; }

        public Matrix(int m, int n)
        {
            this.m = m;
            this.n = n;
            this.data = new double[m, n];
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Random random = new Random();
                    data[i, j] = random.Next() % 10;
                }
            }
        }

        public void FillArr()
        {
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    data[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
        }
        public double this[int x, int y]
        {
            get
            {
                return this.data[x, y];
            }
            set
            {
                this.data[x, y] = value;
            }
        }
        public static Matrix operator *(Matrix matrix, Matrix matrix2)
        {
            if (matrix.N != matrix2.M)
            {
                throw new ArgumentException("Error");
            }
            var result = new Matrix(matrix.M, matrix2.N);
            result.ProcessFunctionOverData((i, j) =>
            {
                for (var k = 0; k < matrix.N; k++)
                {
                    result[i, j] += matrix[i, k] * matrix2[k, j];
                }
            });
            return result;
        }
        public static Matrix operator *(Matrix matrix, double value)
        {
            var result = new Matrix(matrix.M, matrix.N);
            result.ProcessFunctionOverData((i, j) =>
                result[i, j] = matrix[i, j] * value);
            return result;
        }
        public void ShowMatrix()
        {
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    Console.Write($"{data[i, j]} ");
                }
                Console.WriteLine();
            }
        }
        public void Swap(ref int i, ref int k)
        {
            int temp = i;
            i = k;
            k = temp;
        }
        public void ProcessFunctionOverData(Action<int, int> func)
        {
            for (var i = 0; i < this.M; i++)
            {
                for (var j = 0; j < this.N; j++)
                {
                    func(i, j);
                }
            }
        }

        public bool IsSquare { get => this.M == this.N; }
        public Matrix CreateMatrixWithoutColumn(int column)
        {
            if (column < 0 || column >= this.N)
            {
                throw new ArgumentException("Error");
            }
            Matrix matrix = new Matrix(this.M, this.N - 1);
            matrix.ProcessFunctionOverData((i, j) =>
                matrix[i, j] = j < column ? this[i, j] : this[i, j + 1]);
            return matrix;
        }
        public Matrix CreateMatrixWithoutRow(int row)
        {
            if (row < 0 || row >= this.M)
            {
                throw new ArgumentException("Error");
            }
            var result = new Matrix(this.M - 1, this.N);
            result.ProcessFunctionOverData((i, j) =>
                result[i, j] = i < row ? this[i, j] : this[i + 1, j]);
            return result;
        }
        public double Determinant()
        {
            if (!this.IsSquare)
            {
                throw new InvalidOperationException(
                    "Определитель может быть найден только для квадратной матрицы");
            }
            if (this.N == 2)
            {
                return this[0, 0] * this[1, 1] - this[0, 1] * this[1, 0];
            }
            double result = 0;
            for (var j = 0; j < this.N; j++)
            {
                result += (j % 2 == 1 ? 1 : -1) * this[1, j] *
                    this.CreateMatrixWithoutColumn(j).
                    CreateMatrixWithoutRow(1).Determinant();
            }
            return result;
        }
        public Matrix Copy()
        {
            Matrix temp = new Matrix(n, m);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    temp.data[i, j] = data[i, j];
                }
            }
            return temp;
        }
        public void Transa()
        {
            for (int k = 0; k < N; k++)
            {
                for (int t = 0; t < k; t++)
                {
                    var temp = data[t, k];
                    data[t, k] = data[k, t];
                    data[k, t] = temp;
                }
            }
        }
    }
    class Slau
    {

        public Matrix a;// матрица коэффициентов
        public Matrix b;// вектор правой части
        public Matrix x;// вектор решений
        public bool isSolved;  // признак совместности
        public int[] reoder;   // перестановка переменных,

        //	полученная в методе Жордана-Гаусса
        public int rang;// ранг матрицы коэффициентов
        //	конструктор

        public double[,] data=null;

        public int m;
        public int M { get; set; }

        public int n;
        public int N { get; set; }

        public Slau(int m1, int n1)
        {
            m = m1;
            n = n1;
            //	выделение памяти под матрицу коэффициентов
            a = new Matrix(m1, n1);

            //	выделение памяти под вектор свободных членов 
            b = new Matrix(1, m1);

            //	выделение памяти под вектор-решение
            x = new Matrix(1, n1);

            // выделение памяти и заполнение массива

            //	для хранения перестановки переменных 
            reoder = new int[n];

            for (int i = 0; i < n; i++)
            {
                reoder[i] = i;
            }
        }
        public void Inputa()
        {
            Console.WriteLine("Elements matrix koeficentov");
            for (int i = 0; i < m; i++)
            {

                string str = Console.ReadLine();
                string[] s = str.Split(' ');
                for (int j = 0; j < n; j++)
                    a[i, j] = double.Parse(s[j]);

            }

        }
        public void Inputb()
        {
            Console.WriteLine("Elements matrix svobodnih");
            int i = 0;
            string str = Console.ReadLine();
            string[] s = str.Split(' ');
            for (int j = 0; j < m; j++)
                b[i, j] = double.Parse(s[j]);
        }

        //	метод ввода системы уравнения 
        public void Input()
        {

            Console.WriteLine("Matrhix koef: ");
            Inputa();

            Console.WriteLine("Vektor svobodnih: ");
            Inputb();

        }

        //	метод вывода СЛАУ
        public void Print()
        {

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                    Console.Write("" + a[i, j] + "\t");
                Console.WriteLine("\t" + b[0, i]);
            }
            try
            {

                Console.WriteLine("SLAU: ");
                PrintSolution();

            }

            catch (Exception e)
            {
                //	печать возможной ошибки
                Console.WriteLine(e.Message);
            }
        }

        //	метод вывода полученного решения СЛАУ

        public void PrintSolution()
        {
            if (!isSolved)
            {

                Console.WriteLine("Nesovmestna");
                return;

            }
            if (rang < n)
            {
                //	получено общее решение системы 
                for (int i = 0; i < rang; i++)
                {
                    Console.Write("x" + (reoder[i] + 1) + " = " + x[i, 0]);
                    for (int j = 1; j <= n - rang; j++)
                    {
                        if (x[i, j] == 0)
                            continue;
                        if (x[i, j] > 0)
                            Console.Write("+" + x[i, j] + "*x" + (reoder[rang + j - 1] + 1));
                        else
                            Console.Write("" + x[i, j] + "*x" + (reoder[rang + j - 1] + 1));
                    }
                    Console.WriteLine();
                }

            }
            else
            {
                try
                {
                    var check = x[1, 0];
                    for (int i = 0; i < n; i++)
                    {
                        Console.WriteLine(x[i, 0]);
                    }
                }
                catch(Exception)
                {
                    for (int i = 0; i < n; i++)
                    {
                        Console.WriteLine(x[0, i]);
                    }
                }
            }
        }



        //	метод вывода системы уравнения и ее решения 



        //	функция выбора метода решения системы линейных уравнений 
        public void Solve()
        {
            if (m == n) try
                {
                    //	матрица коэффициентов квадратная –
                    //	предоставляется выбор метода решения
                    //	пользователю
                    Console.WriteLine("Choose method");
                    Console.WriteLine("1 - Kramer   2 - Method obratnih   3 - JordanGauss");
                    int i = int.Parse(Console.ReadLine());
                    if (i == 1)
                        Kramer();
                    else if (i == 2)
                        InverseMatrix();
                    else if (i == 3)
                        JordanGauss();
                }
                catch (Exception)
                {
                    //	генерация исключения происходит при
                    //	равенстве нулю определителя матрицы
                    //	коэффициентов. Поэтому осуществляется получение
                    //	общего решения системы
                    JordanGauss();
                }
            else
                //	СЛАУ с прямоугольной матрицей коэффициентов
                //	решается методом Жордана-Гаусса для получения
                //	общего решения
                JordanGauss();
        }


        //	метод Крамера решения СЛАУ 
        public void Kramer()
        {

            //	проверка, является ли матрица прямоугольной 
            if (m != n)

                throw new Exception("Матрица не является квадратной");

            double det = a.Determinant();   // вычисление определителя // матрицы коэффициентов

            //	проверка определенности системы
            if (det == 0)
                throw new Exception("Деление на 0");
            rang = m;
            //	вычисление корней по формулам Крамера
            Matrix temp = a.Copy();
            for (int j = 0; j < n; j++)
            {
                for (int i = 0; i < n; i++)
                    temp[i, j] = b[0, i];
                x[0, j] = temp.Determinant() / det;
                for (int i = 0; i < n; i++)
                    temp[i, j] = a[i, j];
            }
            isSolved = true;
        }
        public Matrix Transa()
        {
            for (int k = 0; k < N; k++)
            {
                for (int t = 0; t < k; t++)
                {
                    var temp = a[t, k];
                    a[t, k] = a[k, t];
                    a[k, t] = temp;
                }
            }
            return a;
        }
        public double obratnaya()
        {

            double det = a.Determinant();

            return (1 / det);
        }

        public void InverseMatrix()
        {
            //	проверка, является ли матрица прямоугольной 
            if (m != n)

                throw new Exception("Матрица не является квадратной");
            //	вычисление обратной матрицы
            Matrix transm = Transa();
            Matrix obr = transm * obratnaya();
            // поскольку для эффективного использования памяти
            //	вектор хранится как строка, требуется получить
            //	соответствующий вектор-столбец посредством
            //	транспонирования


            //	получение решения СЛАУ 

            x = obr * b;
            rang = m;
            isSolved = true;
        }
        public void JordanGauss()
        {
            //	создание копий матрицы коэффициентов и свободных
            //	членов для последующих преобразований

            Matrix A = a;
            Matrix B = b;
            int count_null_cols = 0;

            //	проведение исключений по формулам Жордана-Гаусса 
            for (int i = 0; i < m; i++)
            {
                int k;
                //	исключение по i-ой строке
                //	проверка возможности исключения
                //	по значению ведущего элемента
                if (A[i, i] != 0)
                {

                    //	исключение во всех строках, кроме ведущей 
                    for (k = 0; k < m; k++)

                    {

                        if (k == i) continue;

                        double d = A[k, i] / A[i, i];
                        for (int j = i; j < n; j++)

                            A[k, j] = A[k, j] - d * A[i, j]; B[0, k] = B[0, k] - d * B[0, i];
                    }
                    //	преобразование ведущей строки

                    for (int j = i + 1; j < n; j++)
                        A[i, j] /= A[i, i];
                    //	преобразование i-ого свободного члена
                    B[0, i] /= A[i, i];
                    A[i, i] = 1;

                }
                else
                {

                    //	элемент главной диагонали

                    //	в i-ой строке равен нулю int k;

                    //	поиск ненулевого элемента ниже
                    //	в i-ом столбце
                    for (k = i + 1; k < m; k++)
                        if (A[k, i] != 0)
                            break;

                    if (k == m)
                    {

                        //	все элементы столбца нулевые
                        if (i == n - 1 - count_null_cols)

                        {
                            //	элементов, которые могут быть
                            //	ведущими, больше нет
                            count_null_cols++;
                            break;
                        }
                        //	меняем местами столбцы - текущий и

                        //	последний из непросмотренных
                        for (int j = 0; j < m; j++)
                        {
                            double t = A[j, i];

                            A[j, i] = A[j, n - count_null_cols - 1];
                            A[j, n - count_null_cols - 1] = t;

                        }

                        //	отражаем смену столбцов в перестановке 
                        int te = reoder[i];

                        reoder[i] = reoder[n - count_null_cols - 1];
                        reoder[n - count_null_cols - 1] = te;
                        count_null_cols++;

                        //	далее пытаемся провести исключения
                        //	с той же строкой
                        i--;
                    }


                    else
                    {
                        //	нашли в столбце элемент, который может

                        //	быть ведущим - меняем местами строки 
                        for (int l = 0; l < n; l++)

                        {

                            double t = A[i, l];
                            A[i, l] = A[k, l];
                            A[k, l] = t;

                        }
                        double p = B[0, i];
                        B[0, i] = B[0, k];

                        B[0, k] = p;
                        //	далее пытаемся провести исключения
                        //	с той же строкой
                        i--;
                    }
                }
            }

            //	вычисление ранга матрицы после проведения исключения 
            rang = m < n - count_null_cols ? m : n - count_null_cols;
            //	подсчет количества нулевых строк матрицы
            int null_rows = m - rang;

            //	проверка на несовместность системы –
            //	если в нулевой строке
            //	свободный член не равен нулю
            for (int i = rang; i < m; i++)
                if (B[0, i] != 0)
                {
                    isSolved = false;
                    return;
                }
            // формирование общего решения для совместной СЛАУ
            //	путем переноса свободных переменных в правую часть

            Matrix res = new Matrix(rang, 1 + n - rang);
            for (int i = 0; i < rang; i++)

            {
                res[i, 0] = B[0, i];

                for (int j = rang; j < n; j++)
                    res[i, j - rang + 1] = -A[i, j];
            }

            x = res;
            isSolved = true;

        }
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Kol-vo resheniy: ");
                int m, n;
                m = int.Parse(Console.ReadLine());
                Console.WriteLine("Kol-vo peremennih: ");
                n = int.Parse(Console.ReadLine());
                //	создание объекта системы линейных уравнений
                матрицы.Slau s = new матрицы.Slau(m, n);
                s.Input();
                //	решение системы линейных уравнений
                s.Solve();
                s.Print();

            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
